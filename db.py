from contextlib import contextmanager
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import settings

class Base(declarative_base()):
	__abstract__ = True

class Sound(Base):
	__tablename__ = 't_sound'
	
	hash = sa.Column(sa.String, primary_key = True)
	title = sa.Column(sa.String)
	category = sa.Column(sa.Integer)
	language = sa.Column(sa.Integer)
	is_public = sa.Column(sa.Boolean)

engine = sa.create_engine(settings.DB)
session_factory = sessionmaker(bind = engine)

@contextmanager
def Session():
	if Session._depth > 0:
		yield Session._global
		return
	session = session_factory()
	Session._global = session
	Session._depth += 1
	try:
		yield session
		session.commit()
	except:
		session.rollback()
		raise
	finally:
		session.close()
		Session._global = None
		Session._depth -= 1
Session._global = None
Session._depth = 0
